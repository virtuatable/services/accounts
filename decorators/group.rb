# frozen_string_literal: true

module Decorators
  # This class decorates a group and provides a hash representation.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Group < Virtuatable::Enhancers::Base
    enhances Arkaan::Permissions::Group

    def to_h
      {
        id: id.to_s,
        slug: slug
      }
    end
  end
end
