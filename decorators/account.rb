# frozen_string_literal: true

module Decorators
  # This class decorates an account and provides a hash representation.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Account < Virtuatable::Enhancers::Base
    enhances Arkaan::Account

    def to_h
      {
        id: id.to_s,
        username: username,
        email: email,
        firstname: firstname || '',
        lastname: lastname || '',
        groups: groups.map(&:to_h)
      }
    end
  end
end
