# frozen_string_literal: true

module Services
  # This service holds the logic for creating accounts with provided parameters.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Creation
    include Singleton

    # Runs the service, creating the account and returning it decorated.
    # @params run [Hash] the parameters with which create the account.
    # @return [Decorators::Account] the created account decorated for convenience.
    def run(parameters)
      account = Arkaan::Account.create parse(parameters)
      add_default_groups(account)
      account
    end

    # Adds all the default groups to the given account to give them basic rights.
    # @params [Arkaan::Account] the account to give permissions to.
    def add_default_groups(account)
      account.groups = Arkaan::Permissions::Group.where(is_default: true)
      account.save!
    end

    # Takes the valid fields to include them in the document.
    # @params parameters [Hash] the raw parameters sent from the controller.
    # @return [Hash] the hash purified with only the necessary keys.
    def parse(parameters)
      parameters.select { |k, _| fields.include? k }
    end

    # Returns the fields needed to create an account.
    # @return [Array<String>] the field names included in an account.
    def fields
      %w[
        username
        password
        password_confirmation
        email
        lastname
        firstname
      ]
    end
  end
end
