# frozen_string_literal: true

module Services
  # This module provides methods to list and search for accounts.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Listing
    include Singleton

    # Runs the search engine with the given parameters
    # @param parameters [Hash] the parameters provided in the controller
    # @param requester [Arkaan::Account] the account wanting to get the list
    # @return [Array<Arkaan::Account>] the account corresponding to the criterias
    def run(parameters, requester)
      accounts = if parameters.key? 'search'
                   Arkaan::Account.search(parameters['search'])
                 else
                   Arkaan::Account.all
                 end
      accounts.where(:id.ne => requester.id)
    end
  end
end
