# frozen_string_literal: true

require 'bundler'
Bundler.require :runtime

Virtuatable::Application.load!('accounts')

run Controllers::Accounts
