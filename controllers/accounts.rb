# frozen_string_literal: true

# Main module for all the controllers of the web application
# @author Vincent Courtois <courtois.vincent@outlook.com>
module Controllers
  # Insert more informations about your controller here.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Accounts < Virtuatable::Controllers::Base
    api_route 'post', '/', options: { authenticated: false, premium: true } do
      check_presence('username', 'password', 'password_confirmation', 'email')
      api_created Services::Creation.instance.run(params)
    end

    api_route 'get', '/:id' do
      api_item Arkaan::Account.find(params['id'])
    end

    api_route 'get', '/' do
      api_list Services::Listing.instance.run(params, account)
    end
  end
end
