# frozen_string_literal: true

RSpec.configure do |config|
  DatabaseCleaner[:mongoid].strategy = :truncation, {
    except: %w[
      arkaan_monitoring_routes
      arkaan_monitoring_services
    ]
  }
  config.after(:each) do
    DatabaseCleaner.clean
  end
end
