# frozen_string_literal: true

RSpec.describe Controllers::Accounts do
  def app
    Controllers::Accounts
  end

  include_examples 'GET /'

  include_examples 'GET /:id'

  include_examples 'POST /'
end
