# frozen_string_literal: true

RSpec.shared_examples 'GET /' do
  describe 'List of accounts given search criteria' do
    it_should_behave_like 'a route', 'get', '/'

    let!(:account) { create(:random_administrator) }
    let!(:session) { create(:random_session, account: account) }
    let!(:application) { create(:random_application, creator: account) }
    let!(:payload) do
      {
        app_key: application.app_key,
        session_id: session.session_id
      }
    end

    # Nominal case :
    # - The user makes a request for the lit of all accounts
    # - The API returns a correctly formatted list of accounts
    describe 'When the user wants a list of all accounts' do
      let!(:user_1) { create(:random_account) }
      let!(:user_2) { create(:random_account) }

      before do
        get '/', payload
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expected = {
          count: 2,
          items: [
            {
              id: user_1.id.to_s,
              username: user_1.username,
              email: user_1.email
            },
            {
              id: user_2.id.to_s,
              username: user_2.username,
              email: user_2.email
            }
          ]
        }
        expect(last_response.body).to include_json(expected)
      end
    end

    # Alternative case :
    # - The user makes a request searching for a pattern in accounts
    # - The API returns a correctly formatted list of accounts
    describe 'When the user wants a list of accounts given a search criteria' do
      let!(:user_1) { create(:random_account, username: 'babausse') }
      let!(:user_2) { create(:random_account, username: 'another') }

      before do
        get '/', payload.merge({ search: 'babaus' })
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expected = {
          count: 1,
          items: [
            {
              id: user_1.id.to_s,
              username: user_1.username,
              email: user_1.email
            }
          ]
        }
        expect(last_response.body).to include_json(expected)
      end
    end

    # Alternative case :
    # - The user makes a request searching for a pattern in accounts
    # - The criteria does not match any account
    # - The API correctly returns an empty list of accounts
    describe 'When the user provides a search criteria that does not match any account' do
      let!(:user_1) { create(:random_account, username: 'another') }

      before do
        get '/', payload.merge({ search: 'babaus' })
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expected = { count: 0, items: [] }
        expect(last_response.body).to include_json(expected)
      end
    end

    # Alternative case :
    # - The user makes a request for the lit of all accounts
    # - There are no accounts in the database
    # - The API correctly returns an empty list of accounts
    describe 'When there are no accounts in the database' do
      before do
        get '/', payload
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expected = { count: 0, items: [] }
        expect(last_response.body).to include_json(expected)
      end
    end
  end
end
