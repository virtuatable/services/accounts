# frozen_string_literal: true

RSpec.shared_examples 'POST /' do
  describe 'Creation of an account' do
    let!(:application) { create(:random_premium_app) }
    let!(:existing_account) { create(:random_account) }

    it_should_behave_like 'a route', 'post', '/'

    # Nominal scenario :
    # - The user has provided all the required fields for an account
    # - The fields all respect the required formats and length
    # - The password and its confirmation match
    # - The user and the email address are not already in use
    # - The API creates the account and returns the result
    describe 'The user successfully creates an account' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'best_wizard_xoxo',
          password: 'strong_password',
          password_confirmation: 'strong_password',
          email: 'harry_potter@hogwart.com',
          lastname: 'Potter',
          firstname: 'Harry'
        }
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: Arkaan::Account.find_by(username: 'best_wizard_xoxo').id.to_s,
          username: 'best_wizard_xoxo',
          email: 'harry_potter@hogwart.com',
          lastname: 'Potter',
          firstname: 'Harry',
          groups: []
        )
      end
    end

    # Alternative scenario :
    # - The user provides all fields like in the nominal scenario
    # - The user does not provide a first name
    # - The API creates the account without a first name
    describe 'The user creates an account without the first name' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'the_dude',
          password: 'white_russian',
          password_confirmation: 'white_russian',
          email: 'the_dude@losangeles.com',
          lastname: 'Lebowski'
        }
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: Arkaan::Account.find_by(username: 'the_dude').id.to_s,
          username: 'the_dude',
          email: 'the_dude@losangeles.com',
          lastname: 'Lebowski',
          firstname: '',
          groups: []
        )
      end
    end

    # Alternative scenario :
    # - The user provides all fields like in the nominal scenario
    # - The user does not provide a last name
    # - The API creates the account without a last name
    describe 'The user creates an account without the last name' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'elf_dobby',
          email: 'elf_dobby@hogwart.com',
          password: 'potter_is_good',
          password_confirmation: 'potter_is_good',
          lastname: '',
          firstname: 'Dobby'
        }
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: Arkaan::Account.find_by(username: 'elf_dobby').id.to_s,
          username: 'elf_dobby',
          email: 'elf_dobby@hogwart.com',
          lastname: '',
          firstname: 'Dobby',
          groups: []
        )
      end
    end

    # Alternative scenario :
    # - The user provides all fields like in the nominal scenario
    # - There is already an existing group in the platform data
    # - The API creates the account, assign him to the default groups
    #   and returns the result.
    describe 'The user creates an account with the default groups' do
      let!(:group) { create(:random_group, is_default: true) }

      before do
        post '/', {
          app_key: application.app_key,
          username: 'best_wizard_xoxo',
          password: 'strong_password',
          password_confirmation: 'strong_password',
          email: 'harry_potter@hogwart.com',
          lastname: 'Potter',
          firstname: 'Harry'
        }
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: Arkaan::Account.find_by(username: 'best_wizard_xoxo').id.to_s,
          username: 'best_wizard_xoxo',
          email: 'harry_potter@hogwart.com',
          lastname: 'Potter',
          firstname: 'Harry',
          groups: [
            {
              id: group.id.to_s,
              slug: group.slug
            }
          ]
        )
      end
    end

    # Error scenario :
    # - The user tries to create an account without a username
    # - The API fails and returns an error code
    describe 'The user does not provide a username' do
      before do
        post '/', {
          app_key: application.app_key,
          password: 'strong_password',
          password_confirmation: 'strong_password',
          email: 'harry_potter@hogwart.com',
          lastname: 'Potter',
          firstname: 'Harry'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'username',
          error: 'required'
        )
      end
    end

    # Error scenario :
    # - The user tries to create an account without an email
    # - The API fails and returns an error code
    describe 'The user does not provide an email address' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'best_wizard_xoxo',
          password: 'strong_password',
          password_confirmation: 'strong_password',
          lastname: 'Potter',
          firstname: 'Harry'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'email',
          error: 'required'
        )
      end
    end

    # Error scenario :
    # - The user tries to create an account without a password
    # - The API fails and returns an error code
    describe 'The user does not provide a password' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'best_wizard_xoxo',
          password_confirmation: 'strong_password',
          email: 'harry_potter@hogwart.com',
          lastname: 'Potter',
          firstname: 'Harry'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'password',
          error: 'required'
        )
      end
    end

    # Error scenario :
    # - The user tries to create an account without a password confirmation
    # - The API fails and returns an error code
    describe 'The user does not provide a password confirmation' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'best_wizard_xoxo',
          password: 'strong_password',
          email: 'harry_potter@hogwart.com',
          lastname: 'Potter',
          firstname: 'Harry'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'password_confirmation',
          error: 'required'
        )
      end
    end

    # Error scenario :
    # - The user has provided all the required fields for an account
    # - The provided username is too short (less than six characters)
    # - The API fails and returns an error code
    describe 'The username is too short (< 6 characters)' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'dobby',
          email: 'elf_dobby@hogwart.com',
          password: 'potter_is_good',
          password_confirmation: 'potter_is_good',
          lastname: '',
          firstname: 'Dobby'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'username',
          error: 'minlength'
        )
      end
    end

    # Error Scenario :
    # - The user has provided all the required fields for an account
    # - The provided email address does not meet the format requirements
    # - The API fails and returns an error code
    describe 'The email address does not meet the required format' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'elf_dobby',
          email: 'Dobby email address',
          password: 'potter_is_good',
          password_confirmation: 'potter_is_good',
          lastname: '',
          firstname: 'Dobby'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'email',
          error: 'pattern'
        )
      end
    end

    # Error scenario :
    # - The user has provided all the required fields for an account
    # - The password and its confirmation do not match
    # - The API fails and returns an error code
    describe 'The password confirmation does not match the password' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'the_dude',
          password: 'white_russian',
          password_confirmation: 'black_russion',
          email: 'the_dude@losangeles.com',
          lastname: 'Lebowski'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'password_confirmation',
          error: 'confirmation'
        )
      end
    end

    # Error scenario :
    # - The user has provided all the required fields for an account
    # - The password and the confirmation do match
    # - The email address is valid and not already in use
    # - The user is already in use by another account
    # - The API fails and returns an error code
    describe 'The username is already in use' do
      before do
        post '/', {
          app_key: application.app_key,
          username: existing_account.username,
          password: 'white_russian',
          password_confirmation: 'white_russian',
          email: 'the_dude@losangeles.com',
          lastname: 'Lebowski'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'username',
          error: 'uniq'
        )
      end
    end

    # Error scenario :
    # - The user has provided all the required fields for an account
    # - The password and the confirmation do match
    # - The username is valid and not already in use
    # - The email address is already in use by another account
    # - The API fails and returns an error code
    describe 'The email address is already in use' do
      before do
        post '/', {
          app_key: application.app_key,
          username: 'the_dude',
          password: 'white_russian',
          password_confirmation: 'white_russian',
          email: existing_account.email,
          lastname: 'Lebowski'
        }
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'email',
          error: 'uniq'
        )
      end
    end
  end
end
