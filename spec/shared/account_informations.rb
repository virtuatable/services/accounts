# frozen_string_literal: true

RSpec.shared_examples 'GET /:id' do
  describe 'Informations about an account' do
    let!(:group) { create(:random_admin_group) }
    let!(:account) { create(:random_account, groups: [group]) }
    let!(:session) { create(:random_session, account: account) }
    let!(:application) { create(:random_application) }

    it_should_behave_like 'a route', 'get', '/:id'

    # Nominal scenario :
    # - The user wants to obtain the informations about an account
    # - The account exists in the database
    # - The user has rights to access the informations of the account
    # - The API returns all the informations about the account
    describe 'The account exists with all the needed informations' do
      let!(:searched) { create(:random_account) }

      before do
        get "/#{searched.id}", {
          app_key: application.app_key,
          session_id: session.session_id
        }
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: searched.id.to_s,
          username: searched.username,
          email: searched.email,
          firstname: searched.firstname,
          lastname: searched.lastname,
          groups: []
        )
      end
    end

    # Alternative scenario :
    # - Everything goes as the nominal scenario, but the searched user has no lastname
    # - The API returns the informations with an empty value as lastname
    describe 'The account exists without a lastname' do
      let!(:searched) { create(:random_account, lastname: nil) }

      before do
        get "/#{searched.id}", {
          app_key: application.app_key,
          session_id: session.session_id
        }
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: searched.id.to_s,
          username: searched.username,
          email: searched.email,
          firstname: searched.firstname,
          lastname: '',
          groups: []
        )
      end
    end

    # Alternative scenario :
    # - Everything goes as the nominal scenario, but the searched user has no firstname
    # - The API returns the informations with an empty value as firstname
    describe 'The account exists without a firstname' do
      let!(:searched) { create(:random_account, firstname: nil) }

      before do
        get "/#{searched.id}", {
          app_key: application.app_key,
          session_id: session.session_id
        }
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: searched.id.to_s,
          username: searched.username,
          email: searched.email,
          firstname: '',
          lastname: searched.lastname,
          groups: []
        )
      end
    end

    # Alternative scenario :
    # - Everything goes as the nominal scenario, but the searched user has a group
    # - The API returns the informations with the informations of the group
    describe 'The account exists with a group' do
      let!(:searched) { create(:random_account, groups: [group]) }

      before do
        get "/#{searched.id}", {
          app_key: application.app_key,
          session_id: session.session_id
        }
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          id: searched.id.to_s,
          username: searched.username,
          email: searched.email,
          firstname: searched.firstname,
          lastname: searched.lastname,
          groups: [
            {
              id: group.id.to_s,
              slug: group.slug
            }
          ]
        )
      end
    end

    # Error scenario :
    # - A user wants to obtain informations about an account
    # - The user has rights to access the account
    # - The account does not exist in the database
    # - The API fails and returns an error code
    describe 'The user does not exists' do
      before do
        get '/unknown', {
          app_key: application.app_key,
          session_id: session.session_id
        }
      end
      it 'Returns a 404 (Not Found) status code' do
        expect(last_response.status).to be 404
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 404,
          field: 'id',
          error: 'unknown'
        )
      end
    end
  end
end
